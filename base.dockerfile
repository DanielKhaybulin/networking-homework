FROM rust:1.54.0-slim-buster

RUN apt-get -y update && \
    apt-get install -y python3 python3-dev python3-pip build-essential && \
    rm -rf /var/lib/apt/lists/* && \
    pip3 install pytest "dataclasses; python_version < '3.7'"

ENV PYTHONUNBUFFERED=1

CMD ["bash"]
