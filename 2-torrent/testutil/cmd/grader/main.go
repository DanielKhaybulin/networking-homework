package main

import (
	"flag"
	"log"
	"runtime/debug"
	"testutil"
	"testutil/cli"
	"testutil/tasks"
)

type Subtask struct {
	Name   string
	Points int
	Func   func() bool
}

func main() {
	log.SetPrefix("==== ")

	filter := flag.String("filter", "", "filter for subtasks")
	logDir := flag.String("logs", "", "dir for logs")
	flag.Parse()

	testutil.LogDir = *logDir

	// removing the network just in case
	_ = cli.DockerNetworkRm(testutil.Network)

	subtasks := []Subtask{
		// build images, run simple transfer test
		{
			Name:   "simple-docker-compose",
			Points: 1,
			Func:   tasks.SimpleDockerComposeTest,
		},
		// run simple transfer test with random configs
		{
			Name:   "simple-random",
			Points: 3,
			Func:   tasks.SimpleRandomConfigs,
		},
		// run simple transfer test with some extra cases
		{
			Name:   "simple-extra",
			Points: 2,
			Func:   tasks.SimpleExtraCases,
		},
		//
		{
			Name:   "many-source",
			Points: 1,
			Func:   tasks.ManySource,
		},
		{
			Name:   "restarts",
			Points: 1,
			Func:   tasks.Restarts,
		},
		{
			Name:   "restarts-discovery",
			Points: 1,
			Func:   tasks.RestartsDiscovery,
		},
		{
			Name:   "self-discovery",
			Points: 1,
			Func:   tasks.SelfDiscovery,
		},
	}

	grade := 0
	log.Println("Starting testing...")

	for _, task := range subtasks {
		if *filter != "" && task.Name != *filter {
			log.Printf("SKIPPED task %s", task.Name)
			continue
		}

		if runTask(task) {
			log.Printf("PASSED task %s", task.Name)
			grade += task.Points
		}
	}

	log.Printf("Final grade: %d", grade)
}

func runTask(task Subtask) bool {
	defer func() {
		if r := recover(); r != nil {
			log.Printf("FAILED task %s, panic %v, stack %v", task.Name, r, string(debug.Stack()))
		}
	}()

	log.Printf("Running task %s [%d points]", task.Name, task.Points)

	if !task.Func() {
		log.Printf("FAILED task %s", task.Name)
		return false
	}

	return true
}
