import asyncio
from config import read_config
from storage import FileStorage
from utils import verify_piece
from config import Config
import sys

port_ = 2215
block_num_size = 0


async def sender_handler(
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter,
        fs: FileStorage,
) -> None:
    while True:
        try:
            # Get number of block needed to send
            num = await reader.readexactly(block_num_size)
            num = int.from_bytes(num, 'little')
            # Reading block from file storage
            buf = await fs.read_block(num)

            # print(f'Sending block: {buf}', flush=True)

            # Adding zeroes to the last block if needed
            if len(buf) < fs.file_config.part_size:
                buf += (0).to_bytes(fs.file_config.part_size - len(buf), 'little')

            # Writing block to the buffer
            writer.write(buf)

            await writer.drain()

            sys.stdout.flush()
        except Exception as e:
            print(e)


async def writer_handler(
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter,
        fs: FileStorage,
        config: Config
) -> None:
    for block in range(len(fs.file_config.parts)):
        if not await verify_piece(fs, block, config.file_info.parts[block]):
            # Requesting the block number from peer
            writer.write(block.to_bytes(block_num_size, 'little'))

            await writer.drain()
            # Reading the block from peer
            try:
                buf = await reader.readexactly(fs.file_config.part_size)
            except Exception as e:
                continue

            # Writing the block to file storage
            await fs.write_block(block, buf[:len(await fs.read_block(block))])

            sys.stdout.flush()


def validate_peers(
        peer,
        config: Config
) -> None:
    for peer_ in config.peers:
        if peer_[0] == port_:
            return
    config.peers.append((peer, '2215'))


async def handle_socket(
        reader: asyncio.StreamReader,
        writer: asyncio.StreamWriter,
        config: Config,
        fs: FileStorage
) -> None:

    global block_num_size

    peer, _ = writer.get_extra_info("peername")

    ln = len(fs.file_config.parts)

    while ln:
        ln //= 2
        block_num_size += 1

    block_num_size = block_num_size // 8 + 1

    sys.stdout.flush()

    try:
        await writer_handler(
            reader=reader,
            writer=writer,
            fs=fs,
            config=config
        )
    except Exception as e:
        print(e)

    await sender_handler(
        reader=reader,
        writer=writer,
        fs=fs,
    )

    writer.close()
    await writer.wait_closed()
