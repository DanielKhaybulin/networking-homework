import asyncio
from storage import FileStorage
from config import read_config
from handler import handle_socket
from handler import port_
import sys


def validate_config(
        config
) -> None:
    if not config.file_info.parts:
        config.file_info.parts = list()
    if not config.peers:
        config.peers = list()


async def main() -> None:
    config = read_config()
    validate_config(config)
    file_storage = FileStorage(config.file_info)

    server = await asyncio.start_server(
        lambda r, w: handle_socket(
            reader=r,
            writer=w,
            fs=file_storage,
            config=config
        ),
        '0.0.0.0',
        port_
    )

    address = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {address}', flush=True)
    sys.stdout.flush()
    await asyncio.gather(
        server.serve_forever(),
        background_routine(
            config=config,
            fs=file_storage
        ),
    )


async def background_routine(
        config,
        fs: FileStorage
) -> None:
    # ╰( ͡° ͜ʖ ͡° )つ──☆*:・ﾟ write your code here
    if not config.peers:
        return

    # remember to use asyncio.sleep() and NOT time.sleep() for sleeping:
    await asyncio.sleep(1)

    # ╰( ͡° ͜ʖ ͡° )つ──☆*:・ﾟ write your code here
    # for example, you can use this function to connect to other peers
    tasks = list()

    for peer in config.peers:
        try:
            reader, writer = await asyncio.open_connection(peer, port_)
            tasks.append(
                asyncio.create_task(
                    handle_socket(
                        reader=reader,
                        writer=writer,
                        config=config,
                        fs=fs
                    )
                )
            )
        except Exception as e:
            print(e)
    await asyncio.gather(*tasks)


asyncio.run(main())
