from dataclasses import dataclass, field


class ParsingException(Exception):
    """
    General parsing error. Raised whenever parser cannot parse data correctly
    """
    pass


class IntegrityException(Exception):
    """
    Provided checksum does not match with actual checksum of the packet
    """
    pass


@dataclass
class ProtocolPacket:
    data: bytes

    def hash(self):
        # TODO: Use hash function from RFC
        pass


@dataclass
class Flags:
    value: int
    dont_fragment: bool = field(init=False)
    more_fragments: bool = field(init=False)

    def __post_init__(self):
        # TODO: update dont_fragment and more_fragments fields after hex_value is added
        self.dont_fragment = None  # Change this line
        self.more_fragments = None  # Change this line


@dataclass
class IPPacket:
    version: int
    ihl: int
    dscp: int
    ecn: int
    total_length: int
    identification: int
    flags: Flags
    fragment_offset: int
    ttl: int
    protocol: int
    header_checksum: int
    source_ip: int
    destination_ip: int
    data: ProtocolPacket

    @staticmethod
    def from_bytes(data: bytes) -> "IPPacket":
        # TODO: write your code, converting `data` byte array into IPPacket class instance.
        # Packet is either correct RFC791 packet or invalid. If packet is invalid then raise ParsingException with
        # human-readable message. If byte sequence is valid IP packet but header checksum does not maches actual
        # checksum, raise IntegrityException.
        pass

    def to_bytes(self) -> bytes:
        # TODO: write your code
        # Dump given structure to vds
        pass
