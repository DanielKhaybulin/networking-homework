use std::env;
use std::fmt::{Display, Formatter};
use std::path::Path;
use std::process::Command;

use clap::{value_t, App, AppSettings, Arg, ArgMatches, SubCommand};
use dslib::pynode::PyNodeFactory;
use dslib::test::TestSuite;
use serde::{Deserialize, Serialize};
use sugars::{rc, refcell};

use crate::tests::{
    test_message_delivered_across_networks, test_message_delivered_on_same_network,
    test_message_delivered_through_networks, test_with_empty_routing_information,
    test_with_router_failure, test_with_router_failure_different_masks,
};

mod tests;
mod topologies;

// MAIN --------------------------------------------------------------------------------------------
fn run_parser_tests(matches: &ArgMatches) -> i32 {
    let solution_path = Path::new(matches.value_of("solution_path").unwrap());
    let solution_path_canon = solution_path.canonicalize().unwrap();

    let solution_directory = solution_path_canon.parent().unwrap().to_str().unwrap();

    let homework_directory = solution_path.parent().unwrap();

    let test_directory = homework_directory.join("test");

    let _seed = value_t!(matches.value_of("seed"), u64).unwrap();

    let dslib_path = matches.value_of("dslib_path").unwrap();

    env::set_var(
        "PYTHONPATH",
        format!("{}/python:{}", dslib_path, solution_directory),
    );

    Command::new("pytest")
        .arg(test_directory.to_str().unwrap())
        .status()
        .expect("Failed to start pytest")
        .code()
        .unwrap()
}

#[derive(Copy, Clone)]
struct TestConfig<'a> {
    router_f: &'a PyNodeFactory,
    node_f: &'a PyNodeFactory,
    seed: u64,
}

#[derive(Serialize, Deserialize, Hash, Eq, PartialEq, Clone, Debug)]
struct Payload<'a> {
    src_addr: &'a str,
    dst_addr: &'a str,
    msg_id: u32,
    payload: &'a str,
}

impl<'a> Display for Payload<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&*serde_json::to_string(&self).unwrap())
    }
}

fn run_router_tests(matches: &ArgMatches) {
    let _solution_path = matches.value_of("solution_path").unwrap();
    let _seed = value_t!(matches.value_of("seed"), u64).unwrap();
    let _dslib_path = matches.value_of("dslib_path").unwrap();

    env::set_var("PYTHONPATH", format!("{}/python", _dslib_path));
    let sender_f = PyNodeFactory::new(_solution_path, "Router");
    let receiver_f = PyNodeFactory::new(_solution_path, "EndNode");

    let config = TestConfig {
        router_f: &(sender_f),
        node_f: &(receiver_f),
        seed: _seed,
    };

    let mut tests = TestSuite::new();

    tests.add(
        "SIMPLE same_network",
        test_message_delivered_on_same_network,
        config,
    );
    tests.add(
        "LINE across_networks",
        test_message_delivered_across_networks,
        config,
    );
    tests.add(
        "LINE through_networks",
        test_message_delivered_through_networks,
        config,
    );
    tests.add(
        "RING without_routing_info",
        test_with_empty_routing_information,
        config,
    );
    tests.add("RING router_failure", test_with_router_failure, config);
    tests.add(
        "RING router_failure_different_masks",
        test_with_router_failure_different_masks,
        config,
    );

    tests.run();
}

fn main() {
    let solution_path = Arg::with_name("solution_path")
        .short("i")
        .long("impl")
        .value_name("PATH")
        .help("Path to Python file with solution")
        .default_value("../solution.py");
    let seed = Arg::with_name("seed")
        .short("s")
        .long("seed")
        .value_name("SEED")
        .help("Random seed used in tests")
        .default_value("2021");
    let dslib_path = Arg::with_name("dslib_path")
        .short("l")
        .long("lib")
        .value_name("PATH")
        .help("Path to dslib directory")
        .default_value("../../dslib");

    let args = [dslib_path, seed];

    let matches = App::new("IP routing and fragmentation tests")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .subcommand(
            SubCommand::with_name("parser")
                .arg(solution_path.clone().default_value("../parser.py"))
                .args(&args),
        )
        .subcommand(
            SubCommand::with_name("router")
                .arg(solution_path.clone().default_value("../router.py"))
                .args(&args),
        )
        .get_matches();

    match matches.subcommand() {
        ("parser", Some(sub)) => {
            run_parser_tests(sub);
        }
        ("router", Some(sub)) => {
            run_router_tests(sub);
        }
        _ => {
            panic!("Unknown subcommand")
        }
    }
}
